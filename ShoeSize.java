
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.prefs.Preferences;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;



/* ShoeSize - Eric McCreath 2015 - GPL 
 * This class stores a persons shoe size.
 */

public class ShoeSize {
	private static final String SHOESIZEENAME = "SHOESIZE";
	public static final int SHOESIZEMAX = 15;
	public static final int SHOESIZEMIN = 3;

	static final String FILENAME_TXT = "shoesize.txt";
	static final String FILENAME_SER = "shoesize.ser";
	static final String FILENAME_XML = "shoesize.xml";
	static final String FILENAME_JSON = "shoesize.json";

	private Integer shoesize;
	
	SaveType savetype = SaveType.Bespoke;
	
	private enum SaveType{
		Bespoke,
		Serializable,
		Preferences,
		XML,
		JSON;
	}
	
	public class Bespoke {

	
		public Bespoke(){
			
		}
		
		public Integer loadData(String filePath){
			
			Integer value = null;
			
			try{
				BufferedReader b_reader = new BufferedReader(new FileReader(filePath));
				
				value = Integer.valueOf(b_reader.readLine());
				
				// release system resources
				b_reader.close();
			} 
			catch (IOException x) {
				System.err.format("IOException: %s%n", x);
			}
	
			return value;
		}
		
		public void saveData(Integer size,String filePath){
			
			try{
				BufferedWriter b_writer = new BufferedWriter(new FileWriter(filePath));
				
				b_writer.write(size.toString());
				
			
				// release system resources
				b_writer.close();
			} 
			catch (IOException x) {
				System.err.format("IOException: %s%n", x);
			}

		}
	}
	
	public static class PersistDataSerializable implements Serializable{
		Integer shoesize;
		
		public PersistDataSerializable(Integer size) {
			this.shoesize = size;
		}
		
		public PersistDataSerializable() {
			
		}
		
		public void save(String filename) {
			try {
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
				oos.writeObject(this);
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		static public Integer load(String filename) {
			PersistDataSerializable res = new PersistDataSerializable();
			try {
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
				res = (PersistDataSerializable) ois.readObject();
				ois.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			return res.shoesize;
		}

	}
	
	public class PersistDataPreferences {
		Preferences prefs;
		private static final String NAME = "shoesize";
		private static final int DEFAULTTAG = 0;
		
		public PersistDataPreferences(){
			prefs = Preferences.userNodeForPackage(PersistDataPreferences.class);
		}
		
		public void save(Integer i){
			
			prefs.putInt(NAME, i);
		}
		
		public Integer get(){
			
			Integer value = prefs.getInt(NAME, DEFAULTTAG);
			return value;
		}
	}

	public ShoeSize() {
		
		if(savetype == SaveType.Bespoke){
			shoesize = load_bespoke();
		}
		else if(savetype == SaveType.Serializable){
			shoesize = load_Serializable();
		}
		else if(savetype == SaveType.Preferences){
			shoesize = load_preferences();
		}
		else if(savetype == SaveType.XML){
			shoesize = load_xml();
		}
		else if(savetype == SaveType.JSON){
			shoesize = load_json();
		}
		
	}

	public String show() {
		return (shoesize == null ? "" : shoesize.toString());
	}

	public boolean set(Integer v) {
		if (v == null || v >= ShoeSize.SHOESIZEMIN && v <= ShoeSize.SHOESIZEMAX) {
			shoesize = v;
			save();
			return true;
		} else {
			shoesize = null;
			return false;
		}
	}

	static ShoeSize load() {
		// add code here that will load shoe size from a file called "FILENAME"
        return new ShoeSize();
	}

	void save() {
		// add code here that will save shoe size into a file called "FILENAME"
		
		if(savetype == SaveType.Bespoke){
			save_with_bespoke();
		}
		else if(savetype == SaveType.Serializable){
			save_with_Serializable();
		}
		else if(savetype == SaveType.Preferences){
			save_with_preferences();
		}
		else if(savetype == SaveType.XML){
			save_with_xml();
		}
		else if(savetype == SaveType.JSON){
			save_with_json();
		}
		
	}
	
	void save_with_xml(){
		if(shoesize != null){
			try {
				// Create xml document
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = dbf.newDocumentBuilder();
				Document dom=builder.newDocument();
				// set element value
				Element shoesize_element = dom.createElement("shoesize");
				shoesize_element.setTextContent(shoesize.toString());
				//
				TransformerFactory tff = TransformerFactory.newInstance();
				Transformer tf = tff.newTransformer();
				tf.transform(new DOMSource(shoesize_element), new StreamResult(FILENAME_XML));
				
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	Integer load_xml(){
		Integer value = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbf.newDocumentBuilder();
			Document dom=builder.parse(FILENAME_XML);
			Element element = dom.getDocumentElement();
			value = Integer.valueOf(element.getTextContent());
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}
	
	void save_with_json(){
		if(shoesize != null){
			File file = new File(FILENAME_JSON);
			try {
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,false), "UTF-8"));
				writer.write(shoesize.toString());
				writer.close();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	Integer load_json(){
		
		Integer value = null;
		
		try {
			FileInputStream fileInputStream = new FileInputStream(FILENAME_JSON);
			InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
			BufferedReader reader = new BufferedReader(inputStreamReader);
			String s= reader.readLine();
			value = Integer.valueOf(s);
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return value;
	}
	
	void save_with_preferences(){
		if(shoesize != null){
			PersistDataPreferences data = new PersistDataPreferences();
			data.save(shoesize);
		}
	}
	
	Integer load_preferences(){
		PersistDataPreferences data = new PersistDataPreferences();
		Integer value = data.get();
		return value;
	}
	
	void save_with_bespoke(){
		if(shoesize != null){
			Bespoke data = new Bespoke();
			data.saveData(shoesize, FILENAME_TXT);
		}
	}
	
	Integer load_bespoke(){
		Bespoke data = new Bespoke();
		Integer value = data.loadData(FILENAME_TXT);
		return value;
	}
	
	void save_with_Serializable(){
		if(shoesize != null){
			PersistDataSerializable data = new PersistDataSerializable(shoesize);
			data.save(FILENAME_SER);
		}
	}
	
	Integer load_Serializable(){
		return PersistDataSerializable.load(FILENAME_SER);
	}
}
